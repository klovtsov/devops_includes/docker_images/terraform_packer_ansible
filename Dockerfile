FROM python:3.11.3-bullseye
LABEL maintainer="klovtsov"

ENV PACKER_VERSION=1.9.2
ENV PACKER_SHA256SUM=34fe48d0d5f99670af15d8a3b581db7ce9d08093ce37240d7c7b996de7947275

ENV TERRAFORM_VERSION=1.5.3
ENV TERRAFORM_SHA256SUM=5ce4e0fc73d42b79f26ebb8c8d192bdbcff75bdc44e3d66895a48945b6ff5d48

ENV ANSIBLE_VERSION=8.2.0
ENV ANSIBLE_LINT_VERSION=6.17.2

ENV MOLECULE_VERSION=5.1.0
ENV MOLECULE_PLUGINS_VERSION=23.4.1

ENV DOCKER_VERSION=24.0.5

LABEL PACKER_VERSION=${PACKER_VERSION}
LABEL TERRAFORM_VERSION=${TERRAFORM_VERSION}
LABEL ANSIBLE_VERSION=${ANSIBLE_VERSION}
LABEL ANSIBLE_LINT=${ANSIBLE_LINT_VERSION}
LABEL MOLECULE=${MOLECULE_VERSION}
LABEL MOLECULE_PLUGINS=${MOLECULE_PLUGINS_VERSION}
LABEL DOCKER_VERSION=${DOCKER_VERSION}

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
    && apt-get install -y --no-install-recommends gcc linux-headers-amd64 musl-dev openssl ca-certificates git bash wget openssl zip xorriso openssh-client \
    && pip --no-cache-dir install \
    ansible==${ANSIBLE_VERSION} \
    ansible-lint==${ANSIBLE_LINT_VERSION} \
    molecule==${MOLECULE_VERSION} \
    molecule-plugins[docker]==${MOLECULE_PLUGINS_VERSION} \
    && rm -Rf /usr/share/doc && rm -Rf /usr/share/man \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ADD https://releases.hashicorp.com/packer/${PACKER_VERSION}/packer_${PACKER_VERSION}_linux_amd64.zip ./
ADD https://releases.hashicorp.com/packer/${PACKER_VERSION}/packer_${PACKER_VERSION}_SHA256SUMS ./

ADD https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip ./
ADD https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_SHA256SUMS ./

ADD https://download.docker.com/linux/static/stable/x86_64/docker-${DOCKER_VERSION}.tgz ./
RUN tar xzvf docker-${DOCKER_VERSION}.tgz --strip 1 \
    -C /usr/local/bin docker/docker \
    && rm docker-${DOCKER_VERSION}.tgz

RUN sed -i '/.*linux_amd64.zip/!d' packer_${PACKER_VERSION}_SHA256SUMS
RUN sed -i '/.*linux_amd64.zip/!d' terraform_${TERRAFORM_VERSION}_SHA256SUMS
RUN echo "${PACKER_SHA256SUM} packer_${PACKER_VERSION}_linux_amd64.zip" | sha256sum --check --status
RUN echo "${TERRAFORM_SHA256SUM} terraform_${TERRAFORM_VERSION}_linux_amd64.zip" | sha256sum --check --status
RUN unzip packer_${PACKER_VERSION}_linux_amd64.zip -d /bin
RUN rm -f packer_${PACKER_VERSION}_linux_amd64.zip
RUN unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /bin
RUN rm -f terraform_${TERRAFORM_VERSION}_linux_amd64.zip
COPY .terraformrc ~/.terraformrc

ENTRYPOINT ["/bin/sh", "-c"]
